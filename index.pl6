#!/usr/bin/env perl6

use v6.c;

BEGIN {
    use lib './lib';
};

use CI::Test;

my $obj   = CI::Test.new;
my Int $r = $obj.ci_randomize( 'randomize seed' );
( $obj.ci_hex( 1982) ~ ' with rand=' ~ $r ).say;
