use v6.c;
use Test;
use lib 'lib';

use CI::Test;

plan 10;

my $obj = CI::Test.new;

for (0..9) {
    ok $obj.ci_randomize( 'randomize seed' ~ $_ ) >= 0, 'ci_randomize on iteration=' ~ $_;
}

done-testing;