use v6.c;
use Test;
use lib 'lib';

use CI::Test;

plan 100;

my $obj = CI::Test.new;

for (0..99) {
    my $_h = $obj.ci_hex( $_ );
    ok $_h == :16( $_h ), 'ci_hex on ' ~ $_h;
}

done-testing;