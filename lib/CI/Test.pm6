unit class CI::Test;

method ci_randomize( Str $data ) returns Int {
    ( ( 0..$data.chars ).rand.floor );
}

method ci_hex( Int $num ) returns Str {
    ( '0x' ~ $num.base(16) );
}
