#!/bin/bash

RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
NC='\e[0m'
GITVER='undef';
PRECOMP=./lib/.precomp

for STAGE in {0..2}
do

	if [ $STAGE -eq 0 ]
	then
		if [ -d "${PRECOMP}" ]; then
			rm -rf ${PRECOMP}
		else
			echo -e "Skip delete of ${YELLOW}${PRECOMP}${NC} folder: not existed"
		fi
		echo -e "Try to make test execution of ${YELLOW}index.pl6${NC} script"
		perl6 --stagestats index.pl6 >> /dev/null;
	elif [ $STAGE -eq 1 ]
	then
		echo -e "Running ${YELLOW}CI::Test${NC} tests, part 1:"
		perl6 ./t/01.t;
	elif [ $STAGE -eq 2 ]
	then
		echo -e "Running ${YELLOW}CI::Test${NC} tests, part 2:"
		perl6 ./t/02.t
	fi

	if [ $? -eq 0 ]
	then
		echo -e "[ ${GREEN}all tests passed at stage ${STAGE}${DOMAIN}${NC} ]"
	else
		echo -e "[ ${RED}error at stage ${STAGE}${NC} ]"
		exit -1;
	fi
done
