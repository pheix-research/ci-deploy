# Test repository for CI deploy with triggering

Workout on Gitlab CI deploy within this repository.

## Credits

[Gitlab CI](https://docs.gitlab.com/ce/ci/README.html)

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
